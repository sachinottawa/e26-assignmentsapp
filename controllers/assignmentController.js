const Assignment = require('../models/assignmentModel')

//Get all assignments
const getAllAssignments = async (req, res) => {
    try{
        const assignments = await Assignment.find({});
        res.status(200).json(assignments)
    }catch(error){
        res.status(500).send("Error occured")
    }
}

const getAssignmentById = async(req, res) => {
    try{
        const assignment = await Assignment.findById(req.params.assignmentId).exec();
        res.status(200).json(assignment)
    }
    catch(error){
        res.status(404).send("Assignment not found")
    }
}

const addNewAssignment = async(req ,res) => {
    try{
        const assignment = new Assignment(req.body);
        await assignment.save()
        res.status(201).json(assignment)
    }catch(error){
        res.status(400).send("Please check your data")
    }
}


/**
 * TODO: UPDATE REQUEST NOT WORKING
 * @param {*} req 
 * @param {*} res 
 */
const updateAssignment = async(req, res) => {
    try{
        const assignment = await Assignment.findByIdAndUpdate(req.params.assignmentId, req.body, {new: true})
        res.status(200).json(assignment)
    }catch(error){
        res.status(404).send("Assignment not found")
    }
    res.status(404).send("not written")
}

const deleteAssignment = async(req, res) => {
    try{
        await Assignment.findByIdAndDelete(req.params.assignmentId)
    }catch(error){
        res.status(404).send("Assignment not found")
    }
}

module.exports = {
    getAllAssignments,
    getAssignmentById,
    addNewAssignment,
    updateAssignment,
    deleteAssignment
}