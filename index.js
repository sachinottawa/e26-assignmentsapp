require('dotenv').config()
const express = require('express')
const mongoose = require('mongoose');
const assignmentRoutes = require('./routes/assignmentRoutes')
const app = express()
const cors = require('cors')
const port = 3000

app.use(cors())
app.use(express.json())

app.use('/api/v1/assignments', assignmentRoutes)

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})


main().then(()=>console.log("db connected")).catch(err => console.log(err));

async function main() {
    const url = process.env.DB_URL
    const password = process.env.DB_PASSWORD
    const urlWithPassword = url.replace('<password>', password)
  await mongoose.connect(urlWithPassword);
}