const express = require('express')
const { getAllAssignments, getAssignmentById, addNewAssignment, deleteAssignment, updateAssignment } = require('../controllers/assignmentController')
const router = express.Router()

// Get all assignments  - GET - /assignments
router.get('/', getAllAssignments)

// Get one assignment - GET - /:assignmentId
router.get('/:assignmentId', getAssignmentById)

// Add new assignment - POST - 
router.post('/', addNewAssignment)

// Update an assignment - PATCH - /:assignmentId
router.patch('/:assignmentId', updateAssignment)

// Delete an assignment - DELETE - /:assignmentId
router.delete('/:assignmentId', deleteAssignment)

module.exports = router